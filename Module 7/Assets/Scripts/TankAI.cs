﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankAI : MonoBehaviour
{
    public HealthBar healthBar;
    public float maxHealth = 100;
    public float currentHealth;

    Animator anim;
    public GameObject player;

    public GameObject bullet;
    public GameObject turret;

    public GameObject GetPlayer()
    {
        return player;
    }
    // Start is called before the first frame update
    void Start()
    {
        anim = this.GetComponent<Animator>();
        currentHealth = maxHealth;
        healthBar.SetMaxHealth(maxHealth);
    }

    // Update is called once per frame
    void Update()
    {
        if (isTankDead() != true)
        {
           anim.SetFloat("distance", Vector3.Distance(transform.position, player.transform.position));
           anim.SetFloat("health", currentHealth);
        }
    }

    void Fire()
    {
        GameObject b = Instantiate(bullet, turret.transform.position, turret.transform.rotation);
        b.GetComponent<Rigidbody>().AddForce(turret.transform.forward * 500);
        hasFired(b);
    }

    public void StopFiring()
    {
        CancelInvoke("Fire");
    }

    public void StartFiring()
    {
        InvokeRepeating("Fire", 0.5f, 0.5f);
    }

    public void TakeDamage(float damage)
    {
        currentHealth -= damage;
        healthBar.SetHealth(currentHealth);

        if (currentHealth <= 0)
        {
            Destroy(gameObject);
        }
    }

    bool isTankDead()
    {
        if (player != null && gameObject)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public bool hasFired(GameObject bullet)
    {
       if(bullet != null){
            return true;
        }
        else
        {
            return false;
        }
    }
}
