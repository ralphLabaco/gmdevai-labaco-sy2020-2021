﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public GameObject bullet;
    public GameObject turret;
    public float maxHealth = 100;
    public float currentHealth;
    public HealthBar healthBar;

    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;
        healthBar.SetMaxHealth(maxHealth);
    }

    void Fire()
    {
        GameObject b = Instantiate(bullet, turret.transform.position, turret.transform.rotation);
        b.GetComponent<Rigidbody>().AddForce(turret.transform.forward * 800);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Fire();
        }
    }

    public void TakeDamage(float damage)
    {
         currentHealth -= damage;
         healthBar.SetHealth(currentHealth);

        if (isPlayerDead() == true) 
        {
            Destroy(gameObject);
        }
    }

    bool isPlayerDead()
    {
        if (gameObject != null)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}
