﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    public float damage = 10;
	public GameObject explosion;
	void OnCollisionEnter(Collision col)
    {
    	GameObject e = Instantiate(explosion, this.transform.position, Quaternion.identity);
    	Destroy(e,1.5f);
    	Destroy(this.gameObject);
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

	}

    private void OnTriggerEnter(Collider other)
    {

        Player player = other.GetComponent<Player>();
        TankAI tank = other.GetComponent<TankAI>();
        if (player != null)
        {
            player.TakeDamage(damage);
        }
        if (tank != null)
        {
            tank.TakeDamage(damage);
        }
    }

}
