﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnObstacle : MonoBehaviour
{
    public GameObject monster;
    public GameObject target;
    GameObject[] agents;

    // Start is called before the first frame update
    void Start()
    {
        agents = GameObject.FindGameObjectsWithTag("agent");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if(Physics.Raycast(ray.origin, ray.direction, out hit))
            {
                if (Input.GetMouseButtonDown(0))
                {
                    Instantiate(monster, hit.point, monster.transform.rotation);
                } 
                else
                {
                    Instantiate(target, hit.point, target.transform.rotation);
                }
                
                foreach(GameObject a in agents)
                {
                    a.GetComponent<AIController>().DetectNewObstacle(hit.point);
                }
            }
        }
    }
}
