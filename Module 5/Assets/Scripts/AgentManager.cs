﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgentManager : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject agentOne;
    public GameObject agentTwo;
    public GameObject agentThree;
    public float range = 5;
    void Start()
    {
        Debug.Log("Pursue: " + agentOne.name);
        Debug.Log("Evade: " + agentTwo.name);
        Debug.Log("Hide: " + agentThree.name);
    }

    bool checkRange(GameObject agent)
    {
        if (Vector3.Distance(agent.GetComponent<AIControl>().target.transform.position, agent.transform.position) <= range)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (checkRange(agentOne))
        {
            agentOne.GetComponent<AIControl>().Pursue();
        }
        else
        {
            agentOne.GetComponent<AIControl>().Wander();
        }
            
        if (checkRange(agentTwo))
        {
            agentTwo.GetComponent<AIControl>().Evade();
        }
        else
        {
            agentTwo.GetComponent<AIControl>().Wander();
        }

        if (checkRange(agentThree))
        {
            agentThree.GetComponent<AIControl>().Hide();
        }
        else
        {
            Invoke("Wander", 5.0f);
        }
    }

    void Wander()
    {
        agentThree.GetComponent<AIControl>().Wander();
    }
}
