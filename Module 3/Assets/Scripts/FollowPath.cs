﻿using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPath : MonoBehaviour
{
    Transform goal;
    float speed = 5.0f;
    float accuracy = 1.0f;
    float rotSpeed = 2.0f;
    public GameObject wpManager;
    GameObject[] wps;
    GameObject currentNode;
    int currentWaypointIndex = 0;
    Graph graph;
    //bool isSelected;


    int findClosestWaypoint(){
        int closest = 0;
        if(wps.Length == 0){
            return -1;
        }
        
        float lastDist = Vector3.Distance(this.transform.position, wps[0].transform.position);
        for(int i = 1; i < wps.Length; i++){
            float thisDist = Vector3.Distance(this.transform.position, wps[i].transform.position);
            if(lastDist > thisDist && i != currentWaypointIndex){
                closest = i;
            }
        }
        return closest;
    }

    // Start is called before the first frame update
    void Start()
    {
        wps = wpManager.GetComponent<WaypointManager>().waypoints;
        graph = wpManager.GetComponent<WaypointManager>().graph;
        currentNode = wps[findClosestWaypoint()];
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (graph.getPathLength() == 0 || currentWaypointIndex == graph.getPathLength()){
            return;
        }

        currentNode = graph.getPathPoint(currentWaypointIndex);

        if (Vector3.Distance(graph.getPathPoint(currentWaypointIndex).transform.position, 
        transform.position) < accuracy){
            currentWaypointIndex++;
        }

         if (currentWaypointIndex < graph.getPathLength()){
            goal = graph.getPathPoint(currentWaypointIndex).transform;
            
            Vector3 lookAtGoal = new Vector3(goal.position.x, transform.position.y, goal.position.z);
            Vector3 direction = lookAtGoal - this.transform.position;
            this.transform.rotation = Quaternion.Slerp(this.transform.rotation, 
                                                        Quaternion.LookRotation(direction), 
                                                        Time.deltaTime * rotSpeed);
            this.transform.Translate(0, 0, speed * Time.deltaTime);
         }
    }

    public void GoToHelipad(){
        graph.AStar(currentNode = wps[0], wps[0]);
        currentWaypointIndex = 0;
    }

    public void GoToRuins(){
        graph.AStar(currentNode, wps[8]);
        currentWaypointIndex = 0;
    }

    public void GoToFactory(){
        graph.AStar(currentNode, wps[14]);
        currentWaypointIndex = 0;
    
    }
    public void GoToTwinMountains(){
        graph.AStar(currentNode, wps[4]);
        currentWaypointIndex = 0;
    }

    public void GoToBarracks(){
        graph.AStar(currentNode, wps[26]);
        currentWaypointIndex = 0;
    }
    
    public void GoToCommandCenter(){
        graph.AStar(currentNode, wps[7]);
        currentWaypointIndex = 0;
    }

    public void GoToRadar(){
        graph.AStar(currentNode, wps[31]);
        currentWaypointIndex = 0;
    }

    public void GoToMiddle(){
        graph.AStar(currentNode, wps[27]);
        currentWaypointIndex = 0;
    }

    public void GoToOilRefPumps(){
        graph.AStar(currentNode, wps[12]);
        currentWaypointIndex = 0;
    }

    public void GoToTankers(){
        graph.AStar(currentNode, wps[15]);
        currentWaypointIndex = 0;
    }

}
